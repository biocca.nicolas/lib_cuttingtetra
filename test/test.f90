PROGRAM unitTest
    !----------------------------------------------------------------
    ! The purpose of this program is testing the Module LIB_CuttingTetrahedra
    !
    !
    ! GEOMETRIC REPRESENTATION (GMSH)
    ! Note: renumbering to base-1 notation
    !
    !    Tetrahedron:                          Tetrahedron10:
    !
    !                       v
    !                     .
    !                   ,/
    !                  /
    !               2                                     2
    !             ,/|`\                                 ,/|`\
    !           ,/  |  `\                             ,/  |  `\
    !         ,/    '.   `\                         ,6    '.   `5
    !       ,/       |     `\                     ,/       8     `\
    !     ,/         |       `\                 ,/         |       `\
    !    0-----------'.--------1 --> u         0--------4--'.--------1
    !     `\.         |      ,/                 `\.         |      ,/
    !        `\.      |    ,/                      `\.      |    ,9
    !           `\.   '. ,/                           `7.   '. ,/
    !              `\. |/                                `\. |/
    !                 `3                                    `3
    !                    `\.
    !                       ` w
    !
    !
    !
    !----------------------------------------------------------------
    USE LIB_CuttingTETRAS
    IMPLICIT NONE

    ! TESTING VARIABLES DEFINITION
    integer(4), parameter        :: NodEl = 10         ! 2nd order tetrahedra
    integer(4), parameter        :: NodG  = 4          ! tetra vertices
    integer(4), parameter        :: maxJE = 6          ! subelement incidence max size (prism)
    integer(4), parameter        :: maxFN = 3          ! subelement free-node max size
    integer(4), parameter        :: maxIF = 4          ! subelement interface nodes max size
    real(8)                      :: VOF(NodEl)         ! Volume of Fluid @ tetra vertices. 0 --> fluid node; 1--> gas node
    integer(4), dimension(maxJE) :: JE1, JE2           ! subelement incidence
    integer(4)                   :: nJE1,nJE2          ! JE1 and JE2 size
    integer(4)                   :: cutType            ! cutType indicator
    integer(4), dimension(maxFN) :: freeNod1,freeNod2  !
    integer(4)                   :: FN1, FN2           ! [0,3]
    integer(4), dimension(maxIF) :: JEif               ! incidence interface
    integer(4)                   :: nJEif              ! [0,4]

    integer(4)                   :: eType              ! element type tag {1,2} -> pure fluid/gas
                                                       !                    {3} -> mixture


    ! INTERNAL VARIABLES
    integer(4)  :: ierr     ! status code

    VOF = 0.0d0             ! initilize VOF --> note that size(VOF) = 10, 2nd order tetra

    ! VOF(1:4) --> points to vertex node

    ! test 001
    ! all nodes up to 0.50 --> pure fluid element
    VOF(1:4) = (/0.75d0, 0.80d0, 0.90d0, 0.65d0/)
    call get_GeometricIncidence(VOF,JE1,JE2,JEif,nJEif,cutType,nJE1,nJE2,freeNod1,freeNod2,FN1,FN2)
    write(*,*) '------------------------------------------------------------'
    ierr =  printOutput(VOF,JE1,JE2,JEif,nJEif,cutType,nJE1,nJE2,freeNod1,freeNod2,FN1,FN2)
    write(*,*) '------------------------------------------------------------'
    write(*,*)


    ! test 002
    ! all nodes below to 0.50 --> pure gas element
    VOF(1:4) = (/0.75d0, 0.80d0, 0.90d0, 0.65d0/)
    call get_GeometricIncidence(VOF,JE1,JE2,JEif,nJEif,cutType,nJE1,nJE2,freeNod1,freeNod2,FN1,FN2)
    write(*,*) '------------------------------------------------------------'
    ierr =  printOutput(VOF,JE1,JE2,JEif,nJEif,cutType,nJE1,nJE2,freeNod1,freeNod2,FN1,FN2)
    write(*,*) '------------------------------------------------------------'
    write(*,*)

    CONTAINS

    function printOutput(VOF,JE1,JE2,JEif,nJEif,cutType,nJE1,nJE2,freeNod1,freeNod2,FN1,FN2) result(ierr)
    !--------------------------------------------------------------------------
    ! writes formatted output for element with interfaces.
    !
    !--------------------------------------------------------------------------
    USE LIB_CuttingTETRAS, only : get_ElementType
    implicit none
    integer(4), dimension(*) :: JE1,JE2,JEif,freeNod1,freeNod2
    integer(4)               :: nJE1,nJE2,nJEif,FN1,FN2
    integer(4)               :: cutType
    real(8),dimension(*)     :: VOF

    integer(4)      :: ierr



    integer(4)      :: i     ! counter
    integer(4)      :: eType ! element type {1,2,3} --> {fluid,gas,mixture}

    ! output formatting
    character(len=6) , parameter :: fmt1 = '(F8.3)'
    character(len=4),  parameter :: fmt2 = '(I4)'


    ! VOF
    write(*,'(A)',advance='NO')  'VOF = '
    do i=1, 4
        write(*,fmt1) VOF(i)
    end do
    write(*,*)

    ! element type
    eType = get_ElementType(VOF(1:4))
    select case (eType)
        case (1)
            write(*,'(A)') 'eType = 1 --> pure fluid element'
            return
        case (2)
            write(*,'(A)') 'eType = 2 --> pure gas element'
            return
        case (3)
            write(*,'(A)') 'eType = 3 --> mixture element'
    end select

    ! CutType
    write(*,'(A)',advance='NO') 'CutType = '
    write(*,fmt2) cutType

    ! JE1
    write(*,'(A)',advance='NO') 'JE1 = '
    do i=1, nJE1
        write(*,fmt2) JE1(i)
    end do
    write(*,*)

    ! JE2
    write(*,'(A)',advance='NO') 'JE2 = '
    do i=1, nJE2
        write(*,fmt2) JE2(i)
    end do
    write(*,*)

    ! JEif
    write(*,'(A)',advance='NO') 'nJEif = '
    do i=1, nJEif
        write(*,fmt2) JEif(i)
    end do
    write(*,*)

    end function



END PROGRAM
