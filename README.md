# Conforming Tetrahedron Subdivision Algorithm

This repository contains a Fortran module implementing an algorithm for dividing a tetrahedron intersected by a planar interface into conforming sub-tetrahedra. The algorithm is based on the paper "How to Subdivide Pyramids, Prisms, and Hexahedra into Tetrahedra" by Dompierre, Labbé, Vallet, and Camarero (1999). The implementation follows the guidelines in the report "Cutting Tetrahedra by Node Identifiers" by Kramer (2015). The primary goal is to faithfully replicate the algorithm's functionality as outlined in the report and ensure that all geometric features of the sub-tetrahedra are accurately defined and tracked when a planar surface cuts a tetrahedron.

## Introduction

The problem of conformal decomposition of tetrahedral meshes arises in enriched finite element methods, specifically in the context of the Conformal Decomposition Finite Element Method (CDFEM) and variants of the eXtended Finite Element Method (XFEM). This algorithm provides a solution for efficiently subdividing tetrahedra to maintain conformity with the mesh and accurately capture geometric features.

## Getting Started (WiP)

1. Clone this repository to your local machine:
```bash
git clone https://gitlab.com/biocca.nicolas/lib_cuttingtetra.git

```
2. There are two ways to use the module. 
  - Include the `LIB_CuttingTETRA.f90` module in your Fortran project. 
  - Link the `libcuttingtetras.so` library to your project. An example of this procedure can be found in [test directory](test/) 
3. Use the provided subroutines to perform the conformal subdivision of tetrahedra in your application.

## Usage (WiP)

```fortran
program main
  use LIB_CuttingTETRA

  ! Initialize your tetrahedron and planar interface here

  ! idetify element type by nodal values
  eType = get_ElementType(VOF(1:4))

  ! sub-tetra decomposition 
  call get_GeometricIncidence(VOF,JE1,JE2,JEif,nJEif,cutType,nJE1,nJE2,freeNod1,freeNod2,FN1,FN2)


  ! Your code to work with the resulting sub-tetrahedra
end program main
```

## References

- Dompierre, Labbé, Vallet, and Camarero. "How to Subdivide Pyramids, Prisms and Hexahedra into Tetrahedra" (1999).

- Richard M. J. Kramer. "Cutting Tetrahedra by Node Identifiers" (2015).

## Showcasing Practical Use


The `LIB_CuttingTETRAS` module has been successfully used in free-surface simulation applications, demonstrating its robustness and efficiency in handling complex tetrahedral mesh operations.

<img src="misc/example_application.gif" width="400">
