module LIB_CuttingTETRAS
!implicit none

private
public :: get_Incidence
public :: MoveNodes
public :: get_ElementType
public :: get_MappingLinearFromCuadraticDOFBlock
public :: get_LinearizedBlock
public :: FreeNodesEquation
public :: LocalAssembler
public :: get_normal
public :: get_ExtendedKronecker
public :: FindAnInteger
public :: get_MappingLinearInterfaceElement
!public :: get_FreeNodesEquation


public :: get_GeometricIncidence
public :: cross_product

real(8), parameter:: VOF_int = 0.5d0



Contains

subroutine get_GeometricIncidence(VOF, JE1, JE2, InterfaceNodes, IFNodes,CutType, E1Nodes, E2Nodes, &
                                  FreeNod1, FreeNod2, FreeN1, FreeN2)
!-----------------------------------------------------------------------
! A partir de la VOF de nodos vertices, devuelve las incidencias de cada
! subelemento JE1 y JE2; el tipo de corte; y el numero de nodos de cada
! subelemento.
!
! dependencias:
!               function FindAnInteger
!               function get_EdgeOnes
!               function get_DegNodesOnes
!               function compare_arrays
!
!-----------------------------------------------------------------------

implicit none

! INPUT / OUTPUT Variables
real(8), intent(in) :: VOF(4)                 ! VOF vertices
!integer(4), intent(out) :: IE1, IE2          ! IE
integer(4), intent(out) :: JE1(6), JE2(6)     ! JE : local element incidence
!integer(4), intent(out) :: dJE1(6), dJE2(6)   ! DJE: local element incidence
integer(4), intent(out) :: CutType
integer(4), intent(out) :: E1Nodes, E2Nodes     ! node number for E1 & E2
integer(4), intent(out) :: InterfaceNodes(4)
integer(4), intent(out) :: FreeNod1(3), FreeNod2(3)
integer(4), intent(out) :: FreeN1, FreeN2
integer(4), intent(out) :: IFNodes
!-----------------------------------------------------------------------

! Internal variables
integer(4) DegNodeOnes(4)
integer(4) ID_CutType4(4)
integer(4) ID_CutType3(6)
real(8) tol
integer(4) EdgeOnes(6)
!integer(4)  JE1(6), JE2(6)
!integer(4) dJE1(6), dJE2(6)        ! use in case you want incidence with duplicated nodes
integer(4) DegNodes
integer(4) DegNodesOnes(4)
!-----------------------------------------------------------------------

tol = 0.00000000001d0

Where ( (VOF > (VOF_int-tol)) .and. (VOF < VOF_int + tol) )
    DegNodeOnes = 1
Else Where
    DegNodeOnes = 0
End Where

! sum(DegNodeOnes) = degenerate-node number
! degenerate-node = 1    --> Cut Type 3
! degenerate-node = 2    --> Cut Type 4
! degenerate-node = 0    --> Cut Type 1 or 2

DegNodes = sum(DegNodeOnes)

select case (DegNodes)

    case(2)     ! Cut Type 4
        ! FindAnInteger(1,DegNodeOnes,size(DegNodeOnes))
        !   [1 2]    --> E1 sum = 3
        !   [1 3]    --> E2 sum = 4
        !   [1 4]    --> E3 sum = 5
        !   [2 3]    --> E4 sum = 5
        !   [2 4]    --> E5 sum = 6
        !   [3 4]    --> E6 sum = 7
        E1Nodes = 4          ! TETRA
        E2Nodes = 4          ! TETRA
        FreeN1  = 3
        FreeN2  = 3
        CutType = 4
        IFNodes = 3
        ID_CutType4 = FindAnInteger(1, DegNodeOnes, 4)
        select case (sum(ID_CutType4))
            case (3)    ! Cut Type 4 -->  E1
                JE1 = (/1, 10, 2, 3, 0, 0/)
                JE2 = (/1, 2, 10, 4, 0, 0/)
                FreeNod1 = (/5, 6, 7/)
                FreeNod2 = (/5, 8, 9/)
                interfaceNodes = (/1, 2, 10, 0/)
!                dJE1 = (/1, 10, 2, 3, 0, 0/)
!                dJE2 = (/11, 12, 20, 4, 0, 0/)
            case (4)    ! Cut Type 4 --> E2
                JE1 = (/1, 4, 9, 3, 0, 0/)
                JE2 = (/1, 9, 2, 3, 0, 0/)
                FreeNod1 = (/7, 8, 10/)
                FreeNod2 = (/5, 6, 7/)
                interfaceNodes = (/1, 3, 9, 0/)
!                dJE1 = (/1, 4, 9, 3, 0, 0/)
!                dJE2 = (/11, 19, 2, 13, 0, 0/)
            case (5)
                If (ID_CutType4(1) == 1) then ! Cut Type 4 --> E3
                    JE1 = (/1, 4, 6, 3, 0, 0/)
                    JE2 = (/1, 6, 4, 2, 0, 0/)
                    FreeNod1 = (/7, 8, 10/)
                    FreeNod2 = (/5, 8, 9/)
                    interfaceNodes = (/1, 6, 4, 0/)
!                    dJE1 = (/1, 4, 6, 3, 0, 0/)
!                    dJE2 = (/11, 16, 14, 2, 0, 0/)
                end if
                If (ID_CutType4(1) == 2) then ! Cut Type 4 --> E4
                    JE1 = (/1, 8, 2, 3, 0, 0/)
                    JE2 = (/2, 8, 4, 3, 0, 0/)
                    FreeNod1 = (/5, 6, 7/)
                    FreeNod2 = (/6, 9, 10/)
                    interfaceNodes = (/2, 3, 8, 0/)
!                    dJE1 = (/1, 8, 2, 3, 0, 0/)
!                    dJE2 = (/12, 18, 4, 13, 0, 0/)
                end if
            case (6)    ! Cut Type 4 --> E5
                JE1 = (/1, 2, 7, 4, 0, 0/)
                JE2 = (/2, 3, 7, 4, 0, 0/)
                FreeNod1 = (/5, 8, 9/)
                FreeNod2 = (/6, 9, 10/)
                interfaceNodes = (/2, 7, 4, 0/)
!                dJE1 = (/1, 2, 7, 4, 0, 0/)
!                dJE2 = (/12, 3, 17, 14, 0, 0/)
            case (7)    ! Cut Type 4 --> E6
                JE1 = (/1, 4, 5, 3, 0, 0/)
                JE2 = (/2, 5, 4, 3, 0, 0/)
                FreeNod1 = (/7, 8, 10/)
                FreeNod2 = (/6, 9, 10/)
                interfaceNodes = (/3, 4, 5, 0/)
!                dJE1 = (/1, 4, 5, 3, 0, 0/)
!                dJE2 = (/2, 15, 14, 13, 0, 0/)
        end select
    case(1)     ! Cut Type 3
        E1Nodes = 4      ! TETRA
        E2Nodes = 5      ! PYRAMID
        FreeN1  = 1
        FreeN2  = 3
        CutType = 3
        IFNodes = 3
        EdgeOnes = get_EdgeOnes(VOF)
        DegNodesOnes = get_DegNodesOnes(VOF)
        ID_CutType3 = FindAnInteger(1,DegNodesOnes,4)
        select case (ID_CutType3(1))
            case (1)
                if ( compare_arrays(EdgeOnes,(/0,0,0,1,0,1/))) then ! Cut Type 3 --> E4/E6
                    JE1 = (/1, 10, 6, 3, 0, 0/)
                    JE2 = (/2, 4, 10, 6, 1, 0/)
                    FreeNod1 = (/7, 0, 0/)
                    FreeNod2 = (/5, 8, 9/)
                    interfaceNodes = (/1, 6, 10, 0/)
!                    dJE1 = (/1, 10, 6, 3, 0, 0/)
!                    dJE2 = (/2, 4, 20, 16, 11, 0/)
                end if
                if ( compare_arrays(EdgeOnes,(/0,0,0,1,1,0/))) then ! Cut Type 3 --> E4/E5
                    JE1 = (/1, 6, 9, 2, 0, 0/)
                    JE2 = (/3, 6, 9, 4, 1, 0/)
                    FreeNod1 = (/5, 0, 0/)
                    FreeNod2 = (/7, 8, 10/)
                    interfaceNodes = (/1, 9, 6, 0/)
!                    dJE1 = (/1, 6, 9, 2, 0, 0/)
!                    dJE2 = (/3, 16, 19, 4, 11, 0/)
                end if
                if ( compare_arrays(EdgeOnes,(/0,0,0,0,1,1/))) then ! Cut Type 3 --> E5/E6
                    JE1 = (/1, 9, 10, 4, 0, 0/)
                    JE2 = (/2, 9, 10, 3, 1, 0/)
                    FreeNod1 = (/8, 0, 0/)
                    FreeNod2 = (/5, 6, 7/)
                    interfaceNodes = (/1, 10, 9, 0/)
!                    dJE1 = (/1, 9, 10, 4, 0, 0/)
!                    dJE2 = (/2, 19, 20, 3, 11, 0/)
                end if
            case (2)
                if ( compare_arrays(EdgeOnes,(/0,1,0,0,0,1/))) then ! Cut Type 3 --> E2/E6
                    JE1 = (/2, 7, 10, 3, 0, 0/)
                    JE2 = (/1, 7, 10, 4, 2, 0/)
                    FreeNod1 = (/6, 0, 0/)
                    FreeNod2 = (/5, 8, 9/)
                    interfaceNodes = (/2, 10, 7, 0/)
!                    dJE1 = (/2, 7, 10, 3, 0, 0/)
!                    dJE2 = (/1, 17, 20, 4, 12, 0/)
                end if
                if ( compare_arrays(EdgeOnes,(/0,1,1,0,0,0/))) then ! Cut Type 3 --> E2/E3
                    JE1 = (/1, 8, 2, 7, 0, 0/)
                    JE2 = (/3, 4, 8, 7, 2, 0/)
                    FreeNod1 = (/5, 0, 0/)
                    FreeNod2 = (/6, 9, 10/)
                    interfaceNodes = (/2, 7, 8, 0/)
!                    dJE1 = (/1, 8, 2, 7, 0, 0/)
!                    dJE2 = (/3, 4, 18, 17, 12, 0/)
                end if
                if ( compare_arrays(EdgeOnes,(/0,0,1,0,0,1/))) then ! Cut Type 3 --> E3/E6
                    JE1 = (/2, 10, 8, 4, 0, 0/)
                    JE2 = (/1, 3, 10, 8, 2, 0/)
                    FreeNod1 = (/9, 0, 0/)
                    FreeNod2 = (/5, 6, 7/)
                    interfaceNodes = (/2, 8, 10, 0/)
!                    dJE1 = (/2, 10, 8, 4, 0, 0/)
!                    dJE2 = (/1, 3, 20, 18, 12, 0/)
                end if
            case (3)
                if ( compare_arrays(EdgeOnes,(/1,0,1,0,0,0/))) then ! Cut Type 3 --> E1/E3
                    JE1 = (/1, 8, 5, 3, 0, 0/)
                    JE2 = (/2, 5, 8, 4, 3, 0/)
                    FreeNod1 = (/7, 0, 0/)
                    FreeNod2 = (/6, 9, 10/)
                    interfaceNodes = (/3, 8, 5, 0/)
!                    dJE1 = (/1, 8, 5, 3, 0, 0/)
!                    dJE2 = (/2, 15, 18, 4, 13, 0/)
                end if
                if ( compare_arrays(EdgeOnes,(/1,0,0,0,1,0/))) then ! Cut Type 3 --> E1/E5
                    JE1 = (/2, 5, 9, 3, 0, 0/)
                    JE2 = (/1, 4, 9, 5, 3, 0/)
                    FreeNod1 = (/6, 0, 0/)
                    FreeNod2 = (/7, 8, 10/)
                    interfaceNodes = (/3, 5, 9, 0/)
!                    dJE1 = (/2, 5, 9, 3, 0, 0/)
!                    dJE2 = (/1, 4, 19, 15, 13, 0/)
                end if
                if ( compare_arrays(EdgeOnes,(/0,0,1,0,1,0/))) then ! Cut Type 3 --> E3/E5
                    JE1 = (/3, 4, 8, 9, 0, 0/)
                    JE2 = (/1, 8, 9, 2, 3, 0/)
                    FreeNod1 = (/10, 0, 0/)
                    FreeNod2 = (/5, 6, 7/)
                    interfaceNodes = (/3, 9, 8, 0/)
!                    dJE1 = (/3, 4, 8, 9, 0, 0/)
!                    dJE2 = (/1, 18, 19, 2, 13, 0/)
                end if
            case (4)
                if ( compare_arrays(EdgeOnes,(/0,1,0,1,0,0/))) then ! Cut Type 3 --> E2/E4
                    JE1 = (/3, 7, 6, 4, 0, 0/)
                    JE2 = (/1, 2, 6, 7, 4, 0/)
                    FreeNod1 = (/10, 0, 0/)
                    FreeNod2 = (/5, 8, 9/)
                    interfaceNodes = (/4, 7, 6, 0/)
!                    dJE1 = (/3, 7, 6, 4, 0, 0/)
!                    dJE2 = (/1, 2, 16, 17, 14, 0/)
                end if
                if ( compare_arrays(EdgeOnes,(/1,1,0,0,0,0/))) then ! Cut Type 3 --> E1/E2
                    JE1 = (/1, 5, 7, 4, 0, 0/)
                    JE2 = (/2, 3, 7, 5, 4, 0/)
                    FreeNod1 = (/8, 0, 0/)
                    FreeNod2 = (/6, 9, 10/)
                    interfaceNodes = (/4, 5, 7, 0/)
!                    dJE1 = (/1, 5, 7, 4, 0, 0/)
!                    dJE2 = (/2, 3, 17, 15, 14, 0/)
                end if
                if ( compare_arrays(EdgeOnes,(/1,0,0,1,0,0/))) then ! Cut Type 3 --> E1/E4
                    JE1 = (/2, 6, 5, 4, 0, 0/)
                    JE2 = (/1, 5, 6, 3, 4, 0/)
                    FreeNod1 = (/9, 0, 0/)
                    FreeNod2 = (/7, 8, 10/)
                    interfaceNodes = (/4, 6, 5, 0/)
!                    dJE1 = (/2, 6, 5, 4, 0, 0/)
!                    dJE2 = (/1, 15, 16, 3, 14, 0/)
                end if
        end select

    case(0)     ! Cut Type 1 or 2
        EdgeOnes = get_EdgeOnes(VOF)
        select case (sum(EdgeOnes))
            case(4)  ! Cut Type 2
                E1Nodes = 6     ! PRISM
                E2Nodes = 6     ! PRISM
                FreeN1  = 1
                FreeN2  = 1
                CutType = 2
                IFNodes = 4
                if ( compare_arrays(EdgeOnes,(/0,1,1,1,1,0/))) then ! 1st case in documentation
                    JE1 = (/4, 9, 8, 3, 6, 7/)
                    JE2 = (/1, 7, 8, 2, 6, 9/)
                    FreeNod1 = (/10, 0, 0/)
                    FreeNod2 = (/5, 0, 0/)
                    interfaceNodes = (/6, 9, 8, 7/)
!                    dJE1 = (/4, 9, 8, 3, 6, 7/)
!                    dJE2 = (/1, 17, 18, 2, 16, 19/)
                end if
                if ( compare_arrays(EdgeOnes,(/1,1,0,0,1,1/))) then ! 2nd case in documentation
                    JE1 = (/1, 5, 7, 4, 9, 10/)
                    JE2 = (/7, 3, 10, 5, 2, 9/)
                    FreeNod1 = (/8, 0, 0/)
                    FreeNod2 = (/6, 0, 0/)
                    interfaceNodes = (/5, 7, 10, 9/)
!                    dJE1 = (/1, 5, 7, 4, 9, 10/)
!                    dJE2 = (/17, 3, 20, 15, 2, 19/)
                end if
                if ( compare_arrays(EdgeOnes,(/1,0,1,1,0,1/))) then ! 3rd case in documentation
                    JE1 = (/5, 2, 6, 8, 4, 10/)
                    JE2 = (/1, 8, 5, 3, 10, 6/)
                    FreeNod1 = (/9, 0, 0/)
                    FreeNod2 = (/7, 0, 0/)
                    interfaceNodes = (/5, 8, 10, 6/)
!                    dJE1 = (/5, 2, 6, 8, 4, 10/)
!                    dJE2 = (/1, 18, 15, 3, 20, 16/)
                end if
            case(3)  ! Cut Type 1
                E1Nodes = 4     ! TETRA
                E2Nodes = 6     ! PRISM
                FreeN1  = 0
                FreeN2  = 3
                CutType = 1
                IFNodes = 3
                if ( compare_arrays(EdgeOnes,(/1,1,1,0,0,0/))) then ! N1
!                    print*, "I'm N1"
                    JE1 = (/1, 5, 7, 8, 0, 0/)
                    JE2 = (/8, 5, 7, 4, 2, 3/)
                    FreeNod1 = (/0, 0, 0/)
                    FreeNod2 = (/6, 9, 10/)
                    interfaceNodes = (/5, 7, 8, 0/)
!                    dJE1 = (/1, 5, 7, 8, 0, 0/)
!                    dJE2 = (/15, 17, 18, 2, 3, 4/)
                end if
                if ( compare_arrays(EdgeOnes,(/1,0,0,1,1,0/))) then ! N2
                    JE1 = (/5, 2, 6, 9, 0, 0/)
                    JE2 = (/1, 3, 4, 5, 6, 9/)
                    FreeNod1 = (/0, 0, 0/)
                    FreeNod2 = (/7, 8, 10/)
                    interfaceNodes = (/5, 9, 6, 0/)
!                    dJE1 = (/5, 2, 6, 9, 0, 0/)
!                    dJE2 = (/1, 3, 4, 15, 16, 19/)
                end if
                if ( compare_arrays(EdgeOnes,(/0,1,0,1,0,1/))) then ! N3
                    JE1 = (/7, 6, 3, 10, 0, 0/)
                    JE2 = (/1, 4, 2, 7, 10, 6/)
                    FreeNod1 = (/0, 0, 0/)
                    FreeNod2 = (/5, 8, 9/)
                    interfaceNodes = (/6, 10, 7, 0/)
!                    dJE1 = (/7, 6, 3, 10, 0, 0/)
!                    dJE2 = (/1, 4, 2, 17, 20, 16/)
                end if
                if ( compare_arrays(EdgeOnes,(/0,0,1,0,1,1/))) then ! N4
                    JE1 = (/8, 9, 10, 4, 0, 0/)
                    JE2 = (/1, 2, 3, 8, 9, 10/)
                    FreeNod1 = (/0, 0, 0/)
                    FreeNod2 = (/5, 6, 7/)
                    interfaceNodes = (/8, 10, 9, 0/)
!                    dJE1 = (/8, 9, 10, 4, 0, 0/)
!                    dJE2 = (/1, 2, 3, 18, 19, 20/)
                end if
        end select

    case default
        write(*,*) "WARNING ",DegNodes, "degenenerate node number incompatible"
        write(*,*) "WARNING ",DegNodes, "degenenerate node number incompatible"
        write(*,*) "WARNING ",DegNodes, "degenenerate node number incompatible"
end select

End subroutine

function compare_arrays(a,b) result(equal)
!-----------------------------------------------------------------------
! compara dos arrays de tipo integer. Da como resultado TRUE si son
! iguales, FALSE caso contrario
!
!
!
!-----------------------------------------------------------------------
integer(4), intent(in) :: a(:)
integer(4), intent(in) :: b(:)
logical equal

integer(4) i

do i=1, size(a)
    If ( a(i) == b(i) ) then
        equal = .TRUE.
    Else
        equal = .FALSE.
        return
    end if
end do

end function

function get_EdgeOnes(VOF) result(EdgeOnes)
!-----------------------------------------------------------------------
! calcula un vector donde la posicion de un uno indica el nro de edge
! cortado
!
!
!
!-----------------------------------------------------------------------

real(8), intent(in) :: VOF(4)
integer(4) EdgeOnes(6)
integer(4) i,j,cont

EdgeOnes = 0
cont = 0
do i=1, 4
    do j=i+1, 4
        cont = cont + 1
        if (((VOF(i)>VOF_int).and.(VOF(j)<VOF_int)).or.((VOF(i)<VOF_int).and.(VOF(j)>VOF_int))) then
            EdgeOnes(cont) = 1
        end if
    end do
end do

end function

function get_DegNodesOnes(VOF) result(DegNodesOnes)
!-----------------------------------------------------------------------
! Obtiene DegNodesOnes
! vector con unos en la posicion de un nodo vertice degenerado
! interpretacion:
!                # la interfaz pasa por un nodo vertice   (CutType 3)
!                # la interfaz pasa por dos nodos vertice (CutType 4)
!                # la interfaz no pasa por ningun nodo vertice
!                                                 --> CutType 1 or 2
!-----------------------------------------------------------------------

implicit none

real(8), intent(in)  :: VOF(4)

integer(4) DegNodesOnes(4)
real(8) tol

tol = 0.00000000001d0
DegNodesOnes = 0
Where ( (VOF > (VOF_int-tol)) .and. (VOF < VOF_int + tol)   )
    DegNodesOnes = 1
Else Where
    DegNodesOnes = 0
End Where

end function

function FindAnInteger(ToFind,a,n) result(a_pos)

!-----------------------------------------------------------------------
! This function finds the position of an integer in array (Integer)
! and return another array with the positions of this integer.
!
! example:
! INPUT:
!       n = 6 --> array dimension
!       a = [1 3 6 7 9 3]
!       ToFind = 3
! OUTPUT
!       a_pos = [2 6 0 0 0 0]
!
!-----------------------------------------------------------------------

integer(4), intent(in)  :: ToFind
integer(4), intent(in)  :: a(n)
integer(4), intent(in)  :: n
integer(4) a_pos(n)
integer(4) i, j


a_pos = 0
j = 1

do i=1, n
    if ( a(i) == ToFind ) then
        a_pos(j) = i
        j = j + 1
    end if
end do

end function

subroutine MoveNodes (X,VOF)

!-----------------------------------------------------------------------
!
! Mueve aquellos nodos que pertenecen al edge cortado por la interfaz
! a la posicion de la interfaz en el edge
!
! dependencia:
!               function get_XInterface
!               function get_EdgeOnes
!
!-----------------------------------------------------------------------

! INPUT/OUTPUT VARIABLES
real(8), intent(inout) :: X(3,10)    ! Coordinates for duplicated-Tetras
real(8), intent(in)    :: VOF(4)     ! VOF function for Tetras vertex
!-----------------------------------------------------------------------

! INTERNAL VARIABLES
integer(4) cont
real(8) X_s(3)                         ! Interface-iEdge coordinate
integer(4) i,j
integer(4) EdgeOnes(6)
!-----------------------------------------------------------------------

EdgeOnes = get_EdgeOnes(VOF)

cont = 0
do i=1, 4
    do j=i+1, 4
        cont = cont + 1
        if ( EdgeOnes(cont) == 1 ) then

            select case (cont)
                case (1)
                    X_s = get_XInterface(X(:,1), X(:,2), VOF(1), VOF(2))
                    X(:,5)  =  X_s
                case (2)
                    X_s = get_XInterface(X(:,1), X(:,3), VOF(1), VOF(3))
                    X(:,7)  =  X_s
                case (3)
                    X_s = get_XInterface(X(:,1), X(:,4), VOF(1), VOF(4))
                    X(:,8) =  X_s
                case (4)
                    X_s = get_XInterface(X(:,2), X(:,3), VOF(2), VOF(3))
                    X(:,6) =  X_s
                case (5)
                    X_s = get_XInterface(X(:,2), X(:,4), VOF(2), VOF(4))
                    X(:,9) =  X_s
                case (6)
                    X_s = get_XInterface(X(:,3), X(:,4), VOF(3), VOF(4))
                    X(:,10) = X_s
            end select

        end if
    end do
end do


end subroutine

function get_XInterface(X_i,X_j, VOF_i, VOF_j) result(X_s)

!-----------------------------------------------------------------------
!
! Calcula la posicion donde cae la interfaz x_s sobre un edge determinado
! por el vector X_j - X_i
!
!-----------------------------------------------------------------------

! INPUT/OUTPUT VARIABLES
real(8), intent(in) :: X_i(3)
real(8), intent(in) :: X_j(3)
real(8), intent(in) :: VOF_i
real(8), intent(in) :: VOF_j
real(8) X_s(3)
!-----------------------------------------------------------------------

real(8) s
real(8) VOF_interface

VOF_interface = VOF_int

s = (VOF_interface-VOF_i)/(VOF_j - VOF_i)!*dsqrt(dot_product(X_j - X_i,X_j - X_i)) ! para normalizar s en [0,1]

X_s = X_j*s + X_i*(1.d0 - s)

end function

function get_ElementType(VOF) result(Element_Type)

!-----------------------------------------------------------------------
! this function identifies the element type from nodal VOF values.
! returns Type integer variable
!
!               Type = 1    --> fluid
!               Type = 2    --> gas
!               Type = 3    --> interface // the element has an interface
!
!
!-----------------------------------------------------------------------
implicit none

! INPUT/OUTPUT VARIABLES
real(8), intent(in) :: VOF(:)
integer(4) Element_Type
!-----------------------------------------------------------------------

! INTERNAL VARIABLES
real(8) MinVOF, MaxVOF

!-----------------------------------------------------------------------

MinVOF = minval(VOF)
MaxVOF = maxval(VOF)

Element_Type = 0        ! error

if ( MinVOF>=VOF_int ) then
    Element_Type = 1    ! fluid element
    return
end if

if ( MaxVOF<=VOF_int ) then
    Element_Type = 2    ! gas element
    return
end if

if ( MinVOF<VOF_int .and. MaxVOF>VOF_int ) then
    Element_Type = 3    ! interface element
    return
end if


end function

function get_MappingLinearFromCuadraticDOFBlock() result(Block)
!-----------------------------------------------------------------------
! This function returns the matrix block to linearize any middle-DOF from
! cuadratic tetras.
!
!   p1  p2  p3  p4  p5  p6  p7  p8  p9  p10
!
!   1   1   0   0   -2  0   0   0   0   0       p5      0
!   0   1   1   0   0   -2  0   0   0   0       p6      0
!   1   0   1   0   0   0   -2  0   0   0       p7  =   0
!   1   0   0   1   0   0   0   -2  0   0       p8  =   0
!   0   1   0   1   0   0   0   0   -2  0       p9      0
!   0   0   1   1   0   0   0   0   0   -2      p10     0
!-----------------------------------------------------------------------

implicit none

real(8) Block(6,10) ! only for cuadratic tetras

Block = reshape ((/1.d0, 0.d0, 1.d0, 1.d0, 0.d0, 0.d0, &
                   1.d0, 1.d0, 0.d0, 0.d0, 1.d0, 0.d0, &
                   0.d0, 1.d0, 1.d0, 0.d0, 0.d0, 1.d0, &
                   0.d0, 0.d0, 0.d0, 1.d0, 1.d0, 1.d0, &
                  -2.d0, 0.d0, 0.d0, 0.d0, 0.d0, 0.d0, &
                   0.d0,-2.d0, 0.d0, 0.d0, 0.d0, 0.d0, &
                   0.d0, 0.d0,-2.d0, 0.d0, 0.d0, 0.d0, &
                   0.d0, 0.d0, 0.d0,-2.d0, 0.d0, 0.d0, &
                   0.d0, 0.d0, 0.d0, 0.d0,-2.d0, 0.d0, &
                   0.d0, 0.d0, 0.d0, 0.d0, 0.d0,-2.d0/),(/6,10/))

end function

function get_LinearizedBlock(A,Block) result(A_complete)
!-----------------------------------------------------------------------
! assembles the complete 2nd order block linearized with the complete
! linear block and the mapping block.
! Works only for 1st order TETRAS.
!
!                            [ A  0  ]
!                Complete  = |       |
!                            [ Block ]
!-----------------------------------------------------------------------

implicit none

real(8) A(4,4)
real(8) Block(6,10)
real(8) A_complete (10,10)

!A_complete = 0.d0

A_complete(1:4,1:4)     = A
A_complete(1:4,5:10)    = 0.d0
A_complete(5:10,1:10)   = Block



end function

subroutine get_Incidence(VOF, VOFu, JE, ENodes, FreeNod, FN, InterfaceNodes, IFNodes, CutType)
!-----------------------------------------------------------------------
! calcula los JE, Enodes, FreeNod de forma tal que el fluido siempre
! sea el elemento 1.
! El orden de los nodos interfaz dado en InterfaceNodes sale de forma tal
! que su circulacion apunta de gas a fluido. Esto se debe a que el
! acoplamiento propuesto en la formulacion inpone condiciones de Neumann
! sobre el dominio de gas. Se necesita la normal saliente al dominio de gas
!
!-----------------------------------------------------------------------

implicit none


real(8), intent(in) :: VOF(10)                  ! Volume of Fluid
real(8) VOFu(10)                                ! Volume of Fluid updated at coordinates
integer(4), intent(out) :: JE(6,2)              ! JE(6,1/2) conectividad elemento 1/2
integer(4), intent(out) :: ENodes(2)            ! ENodes(1/2) nro nodos elemento 1/2
integer(4), intent(out) :: FreeNod(3,2)         ! Nodos libres elementos 1/2
integer(4), intent(out) :: FN(2)                ! nro nodos libres elementos 1/2
integer(4), intent(out) :: InterfaceNodes(4)    ! nodos en la interfaz
integer(4), intent(out) :: IFNodes              ! nro de nodos en la interfaz
integer(4), intent(out) :: CutType              ! tipo de corte



! INTERNAL VARIABLES
integer(4) JE1(6), JE2(6)
integer(4) E1Nodes, E2Nodes
integer(4) FreeNod1(3), FreeNod2(3)
integer(4) FreeN1, FreeN2
integer(4) E1Type


call get_GeometricIncidence(VOF(1:4), JE1, JE2, InterfaceNodes, IFNodes, CutType, E1Nodes, E2Nodes, &
                            FreeNod1, FreeNod2, FreeN1, FreeN2)


VOFu = VOF
VOFu(InterfaceNodes(1:IFNodes)) = VOF_int


E1Type = get_ElementType(VOFu(JE1(1:E1Nodes)))

select case (E1Type)

    case (1)   ! E1 --> fluid ; E2 --> gas

        JE(:,1) = JE1
        JE(:,2) = JE2

        ENodes(1) = E1nodes
        ENodes(2) = E2Nodes

        FreeNod(:,1) = FreeNod1
        FreeNod(:,2) = FreeNod2

        FN(1) = FreeN1
        FN(2) = FreeN2

        InterfaceNodes(1:2) = cshift(InterfaceNodes(1:2),1)

        if ( InterfaceNodes(4)/=0 ) then
            InterfaceNodes(3:4) = cshift(InterfaceNodes(3:4),1)
        end if

    case (2)   ! E1 --> gas ; E2 --> fluid

        JE(:,1) = JE2
        JE(:,2) = JE1

        ENodes(1) = E2nodes
        ENodes(2) = E1Nodes

        FreeNod(:,1) = FreeNod2
        FreeNod(:,2) = FreeNod1

        FN(1) = FreeN2
        FN(2) = FreeN1

end select

end subroutine

subroutine FreeNodesEquation(D, FreeNodes, FN, BlockLinear)
!-----------------------------------------------------------------------
! introduce ecuaciones de interpolacion a los nodos libre que quedan
! sin ecuacion
!
! Solo funciona para TETRAS cuadraticos
!
!-----------------------------------------------------------------------
implicit none

integer(4), intent(in) :: FreeNodes(:)
integer(4), intent(in) :: FN
real(8), intent(in) :: BlockLinear(6,10)

real(8) D(10,10)

integer(4) i


if ( FN == 0 ) return ! there is no free nodes to add equation


do i=1, FN

    D(FreeNodes(i),:) = BlockLinear(FreeNodes(i) - 4,:)

end do

end subroutine


function get_FreeNodesEquation(D, FreeNodes, FN, LinearBlock) result(error)
!-----------------------------------------------------------------------
! introduce ecuaciones de interpolacion a los nodos libre que quedan
! sin ecuacion
!
! Solo funciona para TETRAS cuadraticos
!
!-----------------------------------------------------------------------
implicit none

integer(4),  intent(in) :: FreeNodes(:)
integer(4),  intent(in) :: FN
real(8)   ,  intent(in) :: LinearBlock(6,10)

real(8), intent(inout) :: D(:,:)
integer(4) :: error

integer(4) i

error = 0

if ( FN == 0 ) return ! there is no free nodes to add equation


do i=1, FN

    D(FreeNodes(i),:) = LinearBlock(FreeNodes(i) - 4,:)

end do

end function

subroutine LocalAssembler(D, JE, ENodes, JE_c, ENodes_c)
!-----------------------------------------------------------------------
! Esta rutina ensambla las ecuaciones de D con la numeracion del
! subelemento correspondiente (JE), ENodes
!
!
! Solo funciona para TETRAS cuadraticos  D = 10 x 10 dimension
!
!-----------------------------------------------------------------------
implicit none

integer(4), intent(in) :: JE(:)
integer(4), intent(in) :: ENodes
real(8) D(10,10)
integer(4), optional, intent(in) :: JE_c(:)
integer(4), optional, intent(in) :: ENodes_c

! INTERNAL VARIABLES
real(8) Aux(ENodes,ENodes)
real(8) Aux_c(10,10)
!-----------------------------------------------------------------------

if ( present(JE_c) ) then

    Aux_c(1:ENodes,1:Enodes_c) = D(1:ENodes,1:Enodes_c)
    D = 0.d0
    D(JE(1:ENodes),JE_c(1:ENodes_c)) = Aux_c(1:ENodes,1:Enodes_c)

    return

end if


Aux = D(1:ENodes,1:ENodes)

D = 0.d0

D(JE(1:ENodes),JE(1:ENodes)) = Aux


end subroutine

function get_normal(X,indices) result(normal)

implicit none

real(8)   , intent(in) :: X(3,10)
integer(4), intent(in) :: indices(3)
real(8) normal(3)

real(8) X1(3),X2(3),X3(3)
real(8) XA(3),XB(3)
real(8) norm

X1(:) = X(:,indices(1))
X2(:) = X(:,indices(2))
X3(:) = X(:,indices(3))

XA = X2 - X1
XB = X3 - X2

normal = cross_product(XA,XB)
norm = norm2(normal)
normal = normal / norm


end function

subroutine get_ExtendedKronecker(Kronecker, SizeK, ArrayIndex, ArraySize, Value)
!------------
! obtiene la forma matricial de una delta de kronecker extendida, en el siguiente
! sentido
!
!             {  = 1 si i=j y i,j pertencen al conjunto a
!   (I^a)_ij  |
!             {   = 0 en otro caso
!
!
! si Value esta presente, lo que hace es sumar Value en dichas componentes
!
!
!             {  = (I^a)_ij + Value  si i=j y i,j pertencen al conjunto a
!   (I^a)_ij  |
!             {  = 0 en otro caso
!-----------
implicit none
integer(4), intent(in) :: sizeK
integer(4), intent(in) :: ArrayIndex(:)
integer(4), intent(in) :: ArraySize
real(8), optional      :: Value
real(8)                   Kronecker(SizeK,SizeK)

integer(4) i,j

If  ( present(Value) ) then
    do i=1, ArraySize
        j = ArrayIndex(i)
        Kronecker(j,j) = Kronecker(j,j) + Value
    end do
Else
    Kronecker = 0.d0
    do i=1, ArraySize
        j = ArrayIndex(i)
        Kronecker(j,j) = 1.d0
    end do
end If


end subroutine


function get_MappingLinearInterfaceElement(VOF) result(Block)
!-

!-

implicit none

real(8), intent(in) :: VOF(4)
real(8)                Block(6,10) 

integer(4) i,j,cont
real(8) s
real(8) VOF_interface
integer(4) EdgeOnes(6)

VOF_interface = 0.5d0

Block = 0.d0

Block = get_MappingLinearFromCuadraticDOFBlock()

EdgeOnes = get_EdgeOnes(VOF)

cont = 0
do i=1, 4
    do j=i+1, 4
        cont = cont + 1
        
        if ( EdgeOnes(cont) == 1 ) then
        
            select case (cont)
            
                case (1)

                        s = (VOF_interface-VOF(1))/(VOF(2) - VOF(1))
                        Block(1,5) = -1.d0
                        Block(1,1) = 1.d0 - s
                        Block(1,2) = s

                case (2)

                        s = (VOF_interface-VOF(1))/(VOF(3) - VOF(1))
                        Block(2,6) = -1.d0
                        Block(2,2) = 1.d0 - s
                        Block(2,3) = s

                case (3)

                        s = (VOF_interface-VOF(1))/(VOF(4) - VOF(1))
                        Block(3,7) = -1.d0
                        Block(3,1) = 1.d0 - s
                        Block(3,3) = s
                        
                case (4)

                        s = (VOF_interface-VOF(2))/(VOF(3) - VOF(2))
                        Block(4,8) = -1.d0
                        Block(4,1) = 1.d0 - s
                        Block(4,4) = s

                case (5)

                        s = (VOF_interface-VOF(2))/(VOF(4) - VOF(2))
                        Block(5,9) = -1.d0
                        Block(5,2) = 1.d0 - s
                        Block(5,4) = s

                case (6)

                        s = (VOF_interface-VOF(3))/(VOF(4) - VOF(3))
                        Block(6,10) = -1.d0
                        Block(6,3)  = 1.d0 - s
                        Block(6,4)  = s

            end select

        end if

    end do
end do

end function

function cross_product(XA, XB) result(Vn)
!-------------------------------------------------------------
! This function calculates the cross product between two
! vectors in 3-dimensional space.
!
! Inputs:
!   XA, XB - Input vectors (3D)
!
! Outputs:
!   Vn - Cross product of XA and XB
!-------------------------------------------------------------

implicit none

real(8), intent(in) :: XA(3), XB(3)
real(8) :: Vn(3)

Vn(1) = XA(2) * XB(3) - XA(3) * XB(2)
Vn(2) = XA(3) * XB(1) - XA(1) * XB(3)
Vn(3) = XA(1) * XB(2) - XA(2) * XB(1)

end function cross_product


end module LIB_CuttingTETRAS
